<!-- BOOTSTRAP CSS -->
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/dataTables.bootstrap5.css"/>
        
<!-- DATATABLES CSS & JQUERY -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/dataTables.bootstrap5.css" />
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.bootstrap5.css" />



<style>

    body {
        max-width: 100%;
        height: 100%;
        margin: 20 auto;
        background: linear-gradient(45, rgba(251, 238, 238, 0.5), rgba(255, 0, 0, 0.5)) ;
        background-repeat: no-repeat !important;
        background-attachment: fixed;

    }
    
</style>