
<?= $this->extend("layout/master") ?>


<?= $this->section('content') ?>


<div class="container">
    <table id="table_id" class="table caption-top pb-3" style="margin: 0 auto !important">

        <caption>
            TITULO: <?= $title ?>
        </caption>

        <thead>
            <tr>
                <th> nif</th>
                <th> apellido1 </th>
                <th> apellido2 </th>
                <th> nombre </th>
                <th> email </th>
                <th> tipo_tasa </th>
                
            </tr>
        </thead>
        
        <tbody>
            <?php
            foreach ($solicitudes as $solicitud) : ?>
                    <td> <b> <?= $solicitud["nif"] ?> </b>
                    <td> <?= $solicitud["apellido1"] ?> </td>
                    <td> <?= $solicitud["apellido2"] ?> </td>
                    <td> <b> <?= $solicitud["nombre"] ?> </b> </td>
                    <td> <?= $solicitud["email"] ?> </td>
                    <td> <?= $solicitud["tipo_tasa"] ?> </td>
                    <th>
                    <a class="btn btn-light btn-outline-danger" href="<?= site_url('EliminarController') ?>" target="_blank">Borrar</a>
                    </th>
                </tr>

            <?php
            endforeach; ?>
        </tbody>

    </table>
</div>

<?= $this->endsection() ?>