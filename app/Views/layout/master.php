<!DOCTYPE html>
<html lang="es">

<head>

    <!-- METATAGS -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> <?= $title ?> </title>

    <?= $this->include('partials/header') ?>

</head>

<body>


    <!-- Area Dinamica de *sidebar* -->
    <?= $this->renderSection('content') ?>

        <!-- <script src="<= base_url('public/js/script.js') ?>"> </script> -->
        <!-- <= script_tag('public/js/script.js') ?> -->
    <?= $this->include('partials/footer') ?>
</body>

</html>
