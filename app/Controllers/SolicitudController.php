<?php

namespace App\Controllers;

use CodeIgniter\Controller;
use App\Models\SolicitudModel;
use App\Models\SolicitudTabla;


class SolicitudController extends BaseController
{
    private $solicitudes;

    public function __construct(){
        $this->solicitudes = new SolicitudModel();
    }

    public function index()
    {
        $data['solicitudes'] = $this->solicitudes->findAll();        

        $data['title'] = "Solicitudes Totales";

        return view('viewsolicitudes', $data);
    }

    public function solicitudesTabla($valor = "CFSG")
    {     
        $data['solicitudes'] =
            $solicitudes->SELECT('pau.nif', 'pau.apellido1', 'pau.apellido2', 'pau.nombre', 'pau.email', 'ciclos.familia', 'ciclos.tipo_tasa')
                    ->JOIN('ciclos', 'ciclos.id = pau.ciclo', 'RIGHT')
                    ->WHERE(['familia' => $valor])
                    ->findAll();

        $data['title'] = "Solicitudes Totales";
        return view('viewsolicitudes', $data);
    }

}

    