<?php

namespace App\Controllers;

use App\Models\SolicitudModel;
use App\Views\eliminar;

class EliminarController extends BaseController {

    public function __construct(){
    $this->eliminar = new SolicitudModel();
    }

    public function index() {
        $data['eliminar'] = $this->eliminar->findAll();
        return view('eliminar', $data);
    }

    public function eliminar($nif = "73453451F") {

        $eliminar->where('nif', $nif)->delete();
        echo "Acabo de borrar la solicitud del alumno con el siguiente nif $nif";
        return view('viewsolicitudes', $data);
    }

}