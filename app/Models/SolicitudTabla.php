<?php

namespace App\Models;

use CodeIgniter\Model;

class SolicitudTabla extends Model
{
    protected $table = 'ciclos';
    protected $primaryKey = 'id';
}